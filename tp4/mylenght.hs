mylength :: [a] -> Int
mylength [] = 0
mylength (_:xs) = 1 + mylength xs

main :: IO()
main = do
    print(mylength [1,2,3])