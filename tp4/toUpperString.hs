import Data.Char

toUpperString :: String -> String
toUpperString [] = ""
toUpperString (x:xs) =  (toUpper x):toUpperString xs

main :: IO()
main = do
    print(toUpperString "Oui")