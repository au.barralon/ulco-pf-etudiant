mulListe :: Num a => a -> [a] -> [a]
mulListe k [] = []
mulListe k (x:xs) = x*k : mulListe k xs

selectListe :: (Int, Int) -> [Int] -> [Int]
selectListe (x0, x1) [] = []
selectListe (x0, x1) (x:xs) =
    if x >= x0 && x <= x1
    then x : selectListe (x0, x1) xs
    else selectListe (x0, x1) xs

sumListe :: [Int] -> Int
sumListe [] = 0
sumListe (x:xs) = x + sumListe xs

main :: IO ()
main = do
    print $ mulListe 3 [3, 4, 1]
    print $ selectListe (2, 3) [3, 4, 1]
    print $ sumListe [3, 4, 1]