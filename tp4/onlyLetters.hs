import Data.Char
onlyLetters::String -> String
onlyLetters [] = ""
onlyLetters (x:xs) =  if res then [x] ++ onlyLetters xs else onlyLetters xs
    where res = isLetter(x)

main :: IO()
main = do
    print(onlyLetters "foo bar")