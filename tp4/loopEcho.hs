echoloop :: Int -> IO ()
echoloop 0 = putStrLn "loop terminated"
echoloop x = do
    putStr ">"
    str <- getLine
    if str == "" then (putStrLn "empty line") else do
        putStrLn str
    echoloop (x-1)

main :: IO ()
main = echoloop 3