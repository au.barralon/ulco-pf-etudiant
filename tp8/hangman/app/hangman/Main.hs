import System.Random (randomRIO)
import System.Environment
listemot :: [String]
listemot = ["function", "functional programming", "haskell", "list", "pattern matching", "recursion", "tuple", "type system"]

dessinpendu :: [String]
dessinpendu =  ["                    \n\
                \                    \n\
                \                    \n\
                \                    \n\
                \                    \n\
                \                    \n\
                \____________________",
                "                    \n\
                \     |              \n\
                \     |              \n\
                \     |              \n\
                \     |              \n\
                \     |              \n\
                \_____|________________",
                "      ________       \n\
                \     |/       |      \n\
                \     |       (_)     \n\
                \     |               \n\
                \     |               \n\
                \     |               \n\
                \_____|________________",
                "      _________      \n\
                \     |/        |      \n\
                \     |       (é_è)     \n\
                \     |         |      \n\
                \     |         |      \n\
                \     |               \n\
                \_____|________________",
                "      _________      \n\
                \     |/        |      \n\
                \     |       (é_è)     \n\
                \     |        \\|/     \n\
                \     |         |      \n\
                \     |               \n\
                \_____|________________",
                "      _________        \n\
                \     |/        |        \n\
                \     |       (é_è)     \n\
                \     |        \\|/      \n\
                \     |         |       \n\
                \     |        / \\     \n\
                \_____|________________"]


motcache :: [Char] -> [Char]
motcache [] = []
motcache (x:xs) = if x == ' ' then ' ':motcache xs else '?':motcache xs

inputLetter :: String -> IO String
inputLetter lettre = do
    putStrLn lettre
    lettre <- getLine
    return lettre

main :: IO ()
main = do 
    let histo = []
    print $ listemot
    putStrLn $ dessinpendu !! 5
    randomNumber <- randomRIO (0,7)
    let mot = listemot !! randomNumber
    putStrLn $ motcache mot
    l <- getLine
    histo <- new_element : l
    putStrLn $ histo


