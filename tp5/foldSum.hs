foldSum1 :: Num(a) => [a] -> a
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 xs

foldSum2 :: Num(a) => [a] -> a
foldSum2 xs = foldl (+) 0 xs

main :: IO()
main = do 
    print $ foldSum1 [1..4]
    print $ foldSum2 [1..4]
