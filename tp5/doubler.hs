doubler :: [Int] -> [Int]
doubler liste = [x*2 | x <- liste]

main :: IO()
main = do
    print $ doubler [1..4]