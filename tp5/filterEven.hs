filterEven1 :: [Int] -> [Int]
filterEven1 [] = []
filterEven1 (x:xs) = if even x then x : filterEven1 xs else filterEven1 xs

filterEven2 :: [Int] -> [Int]
filterEven2 [] = []
filterEven2 list = filter (even) list

main :: IO()
main = do
    print $ filterEven1 [1..4]
    print $ filterEven2 [1..4]