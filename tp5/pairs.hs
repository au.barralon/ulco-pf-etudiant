pairs :: [Int] -> [Int]
pairs liste =  [x | x <- liste, even x]

main :: IO()
main = do
    print $ pairs [1..4]