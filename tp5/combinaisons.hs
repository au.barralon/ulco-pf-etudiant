combinaisons :: [a] -> [b] -> [(a,b)]
combinaisons l1 l2= [(x, y) | x <- l1, y <- l2]

main :: IO()
main = do
    print $ combinaisons ["pomme", "poire"]  [13, 37, 42]