multiples :: Int -> [Int]
multiples n = [x | x <- [1..n], n `mod` x == 0]

main :: IO()
main = do
    print $ multiples 80