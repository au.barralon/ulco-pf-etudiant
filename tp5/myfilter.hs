import Data.Char

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter f (x:xs) = if f x 
                    then x : myfilter f xs
                    else myfilter f xs

main :: IO()
main = do
    print $ myfilter (even) [1..4]
    print $ myfilter (isLetter) "Bonjour21,"