import Data.Char

mapDoubler1 :: Num a => [a] -> [a]
mapDoubler1 [] = []
mapDoubler1 (x:xs) = 2*x : mapDoubler1 xs

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 n = map (4*) n

mymap :: (a -> b) -> [a] -> [b]
mymap _ [] = []
mymap f (x:xs) = f x : mymap f xs

main :: IO()
main = do
    print $ mapDoubler1 [1..4 :: Int]
    print $ mapDoubler2 [1..4]
    print $ mymap (2*) [1..4 :: Int]   
    print $ mymap (toUpper) "coucou"