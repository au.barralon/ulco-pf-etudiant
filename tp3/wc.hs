import System.Environment
import System.IO

main :: IO()
main = do
    x <- getArgs
    if(length x /=1 )
        then putStrLn "usage: <file>"
        else do
            file <- readFile $head x
            print $words file
            putStrLn ("lines: " ++(show $length (lines file)) )        
            putStrLn ("words: " ++(show $length (words file)) )        
            putStrLn ("lines: " ++(show $length file) )
            putStrLn ("non-whitespace chars: " ++(show $length $concat $words file) )