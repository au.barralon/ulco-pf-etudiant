twice :: [a] -> [a]
twice x = x ++ x

sym :: [a] -> [a]
sym x = x ++ (reverse x)



main :: IO()   
main = do
    print (twice [1,2])
    print (twice "to")
    print (sym [1,2])
    print (sym "to")