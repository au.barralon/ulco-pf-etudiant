{-# LANGUAGE OverloadedStrings #-}

import qualified GI.Gtk as Gtk
import Data.Text

afficheCoucou :: IO()
afficheCoucou = do
    putStrLn "coucou"

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    Gtk.windowSetDefaultSize window 300 300
    Gtk.windowSetTitle window "Hello World!"
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    button <- Gtk.buttonNewWithLabel "Bouton boutonneux"
    Gtk.containerAdd window button

    _ <- Gtk.onButtonClicked button afficheCoucou

    Gtk.widgetShowAll window
    Gtk.main

