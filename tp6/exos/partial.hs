myTake2 :: [Int] -> [Int]
myTake2 liste = take 2 liste

myGet2 :: [Int] -> Int
myGet2 = flip (!!) 2 

main = do
    print (myTake2 [1..4])
    print (myGet2 [1..4])
    print (map (2*) [1..4])
    print (map (\x -> 2*x)[1..4])
    print (map (\x -> x/2)[1..4])
    print (map (\(i, l) -> l : '-' : show i) (zip [1..26] ['a'..'z'] ))
    print (zipWith(\ x y -> y : '-' : show x) [1..26] ['a'..'z'])