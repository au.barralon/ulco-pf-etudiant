plus42 :: Int -> Int
plus42 = (42+)

positif :: Int -> Bool
positif = (>0)

plus42positif :: Int -> Bool
plus42positif = positif . plus42

mul2PlusUn :: Int -> Int
mul2PlusUn = (+1) . (*2)

mul2MoinsUn :: Int -> Int
mul2MoinsUn = (+(-1)) . (*2)

applyTwice :: (a -> a) -> a -> a
applyTwice mul = mul . mul

main :: IO()
main = do
    print (plus42positif 2)
    print (plus42positif (-84))
    print (mul2PlusUn 3)
    print (mul2MoinsUn 3)
    print (applyTwice mul2PlusUn 3)