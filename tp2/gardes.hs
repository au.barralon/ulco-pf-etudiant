fonctionEquiParite :: Int -> String
fonctionEquiParite x
               | even x = "pair"
               | otherwise = "impair"
fonctionEquiSigne :: Int -> String
fonctionEquiSigne x
               | x == 0 = "Nul"
               | x > 0 = "Positif"
               | otherwise = "Negatif"


main :: IO ()
main = do
     putStrLn "Hello !"
     print(fonctionEquiParite 3)
     print(fonctionEquiParite 2)
     print(fonctionEquiSigne 3)
     print(fonctionEquiSigne 0)
     print(fonctionEquiSigne (-2))