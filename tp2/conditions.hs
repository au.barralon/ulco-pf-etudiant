formaterParite :: Int -> String
formaterParite x = if even x then "pair" else "impair"
formaterSigne :: Int -> String
formaterSigne x =
     if x == 0 then "nul" else
          if x < 0 then "negatif" else "positif"

main :: IO ()
main = do
     putStrLn "Hello !"
     print(formaterParite 21)
     print(formaterParite 42)
     print(formaterSigne 21)
     print(formaterSigne 0)
     print(formaterSigne (-2))